#!/usr/bin/env bash

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

puppet_repo="https://yum.puppet.com/puppet5/puppet5-release-el-7.noarch.rpm"
puppet_package="puppet"
puppet_conf_path="/etc/puppetlabs/puppet/puppet.conf"
puppet_environment_path="/etc/puppetlabs/code/environments/"
hosts_path="/etc/hosts"
profile="/etc/profile"

case $1 in

    --install)
        rpm --import "https://yum.puppetlabs.com/RPM-GPG-KEY-puppet"
        yum -f install "${puppet_repo}"
        yum -f install "${puppet_package}"
        source "${profile}"
        ;;
    --uninstall)
        ;;
    *)
        printf "\nPara instalar selecione um --install | --uninstall"
        ;;

esac

printf "\nInsira o FQDN do puppet server: "
read -r puppet_server_fqdn

printf "\nInsira o IP do Puppet Server: "
read -r puppet_server_ip

printf "\nDigite o Environment: "
read - puppet_environment

cat <<EOF >> "${puppet_environment_path}/${puppet_environment}/environment.conf"
modulepath = /etc/puppetlabs/code/environments/${puppet_environment}/modules
EOF

cat <<EOF > ${puppet_conf_path}
# Arquivo gerenciado remotamente.
# Qualquer alteração local será perdida.
#
[main]
  server            = ${puppet_server_fqdn}
  environment       = ${puppet_environment}
  splay             = true
  splaylimit        = 15m
  runinterval       = 30m
  color             = ansi
  strict_variables  = true
  report            = true

EOF

cat <<EOF > ${hosts_path}
### Arquivo gerenciado externamente
### Qualquer alteração local será perdida.
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6

### Puppet Server
${puppet_server_ip} ${puppet_server_fqdn}
EOF

source "/etc/profile"
