### Automate Install Puppet Agent for CentOS

 - Para realizar a instalção do Puppet execute:

```bash
~]# bash puppet_agent.sh --install
```

 - Para remover:

```bash
~]# bash puppet_agent.sh --uninstall
```