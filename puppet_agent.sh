#!/usr/bin/env bash

### RPM Package Manage
rpm="/usr/bin/rpm"

### YUM Package Manage
yum="/usr/bin/yum"

### Program for regex
grep="/usr/bin/grep"

### Program SED
sed="/usr/bin/sed"

### Program for copy
cp="/usr/bin/cp"

### Program for cd
cd="/usr/bin/cd"

### Program for ls
ls="/usr/bin/ls"

### Program for echo
echo="/usr/bin/echo"

### Program for remove directory
rm="/usr/bin/rm"

### Diretório Profile
profile_default=$(. /etc/profile)

### Arquivo de configuração do puppet
puppet_conf="/etc/puppetlabs/puppet/puppet.conf"

### Hosts
hosts_conf="/etc/hosts"

### Diretório de instalação do puppet
puppet_dir="/etc/puppetlabs"

case $1 in

    --install)

        ### Verifica se o pacote já está instalado.
        $rpm -aq |$grep puppet

        if [ $? != 0 ]; then

            ### Instala o pacote da puppetlabs que contem o environment puppet
            $rpm -Uvh https://yum.puppetlabs.com/puppetlabs-release-pc1-el-7.noarch.rpm

            ### Atualiza o sistema operacional com o novo repositório do puppet
            $yum update -y

        else

            ### Aviso de que o pacote do puppet já está instalado.
            $echo -e "\nO Puppet encontra-se instalado!\n"
            exit
        fi

        ### Realiza uma busca na base do YUM para verificar se o pacote puppet-agent existe.
        $yum search all puppet-agent |$grep puppet-agent.x86_64

        ### Teste para verificar se o pacote puppet-agent foi encontrado na base do YUM.
        if [ $? == 0 ]; then

            ### Instala o pacote puppet-agent.
            $yum install puppet-agent.x86_64 -y

            $echo -e "Você está executando o puppet em modo autonomo?:[Y/n]"
            read -r response
            if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]; then

                echo -e "Puppet configurado para o modo Autonomo!"

                #
                # Recarrega o arquivo profile
                #
                source /etc/profile

            else

                ### Copia o arquivo puppet.conf para o diretório do puppet.
                $cp -rf puppet.conf ${puppet_conf}

                ### Copia o arquivo hosts para o diretório /etc.
                $cp -rf hosts ${hosts_conf}

                ### Configuração do Puppet server

                    ### Configuração do FQDN do Puppet Server
                printf "\nInsira o FQDN do Puppet Server: "
                read -r puppet_server
                $sed -i -e "s/\ \ server\ \ \ \ \ \ \ \ \ \ \ \ =/\ \ server\ \ \ \ \ \ \ \ \ \ \ \ =\ ${puppet_server}/g" \
                "${puppet_conf}"

                    ### Configuração do IP do Puppet Server
                printf "\nInsira o IP do Puppet Server: "
                read -r ip_puppet_server
                $echo -e "${ip_puppet_server} ${puppet_server}" >> "/etc/hosts"

                    ### Configuração do Environment para o puppet agent.
                printf "\nDigite o Environment: "
                read -r puppet_environment
                $echo -e "\nConfiguração realizada com sucesso!\n"
                $sed -i -e "s/\ \ environment\ \ \ \ \ \ \ =/\ \ environment\ \ \ \ \ \ \ = ${puppet_environment}/g" \
                "${puppet_conf}"

                ### Teste para verificar se o environment configurado existe
		        $ls "/etc/puppetlabs/code/environments/" |grep "${puppet_environment}" > /dev/null

                if [ $? != 0 ]; then

                        ### Cria o diretório para o environment configurado
                    $cp -rf "/etc/puppetlabs/code/environments/production" \
		            "/etc/puppetlabs/code/environments/${puppet_environment}"

                        ### Configuração do path para execução dos módulos do puppet.
                    $echo -e "modulepath = /etc/puppetlabs/code/environments/${puppet_environment}/modules" \
                    >> "/etc/puppetlabs/code/environments/${puppet_environment}/environment.conf"
                fi

                #
                # Recarrega o arquivo profile
                #
                source /etc/profile
            fi

            ### Configuração do path para execução dos módulos do puppet.
            $echo -e "modulepath = /etc/puppetlabs/code/environments/production/modules" \
            >> "/etc/puppetlabs/code/environments/production/environment.conf"

            #
            # Recarrega o arquivo profile
            #
            source /etc/profile
        fi

        ### EOF
        exit
        ;;

    --uninstall)

        $yum remove puppet-agent -y
        $yum remove puppetlabs-release-pc1-1.1.0-5.el7.noarch -y
        $rm -rf "${puppet_dir}"
        $echo -e "\nRemoção realizada com sucesso!\n"
        ;;
    *)
        $echo -e "O que deseja?: Instalar?: --install Remover?: --uninstall"
        ;;
esac
